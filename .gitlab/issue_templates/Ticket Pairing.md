Title: Pairing Session - Date - Name 1 / Name 2 

#### Overview:

Boom! It's on. You and your pairing partner have 1 hour. Work together on tickets and log them here. Fill this out at the end of your session as a logbook so we can track our progression!


### Tickets Worked On:
+ Ticket:
+ Ticket:


### Post Call Notes:

  Use this section to make any notes of recurring themes or issues or anything generally awesome you learned during this session. The Team lead will review these sections weekly to try and spot any documentation changes or trends that we should be aware of.

/label ~"Team Training"

/assign @lbot (This issue will be assigned to Lee)

/milestone %"Q2 2017 Goal: 15 Pairing Sessions" (Set Milestone)
