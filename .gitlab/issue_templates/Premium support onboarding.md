Title: Premium Support Onboarding - CompanyName

#### Customer overview

+ Company name:

+ Company email domain:

+ Primary Contact:

+ Timezone:

+ Salesforce account:

+ Sales contact:

_(mark issue as confidential and assign to the Support Lead)_


#### Support actions

- [ ] **Support Lead/Zendesk Admin**: Add the organization to the Zendesk ["premium customers trigger"](https://gitlab.zendesk.com/agent/admin/triggers/163814667). *Premium customers only:*
- [ ] **Assigned SE**: Create a [new Zendesk ticket](https://gitlab.zendesk.com/agent/tickets/new/2) using the customers contact email as the "Requester". Apply the following template as the "Description", personalizing where appropriate.

> Title: GitLab - Premium Support for {{COMPANYNAME}}
>
> Hi {{CONTACTNAME}},
>
> As part of your GitLab premium subscription, I'd like to welcome you to GitLab, and let you know how best to reach Support when you need it. Also, please check to see the various services that Premium Support entitles your organization to, at https://about.gitlab.com/features/premium-support .
>
> **Contacting support**
>
> Please use the following contact information when reaching out to GitLab for support.
>
>  + File your support requests via the web form at https://support.gitlab.com
>  + You can also email for general support issues/questions: {{SUBSCRIBERS EMAIL}}
>  + Emergencies (24x7): {{EMERGENCY EMAIL}}
>
> **Training workshops**
>
> You can see a list of our offered training workshops here https://about.gitlab.com/training/
>
> Please let me know which two training workshops would best suit your needs and what date & time is best to deliver these.
>
>

- [ ] **Assigned SE**: Schedule two training workshops with the customer, arrange these via the above Zendesk ticket or via a WebEx call.
