Title: Customer Training - Customer Name - Name of Training Requested

(PLEASE MARK THIS ISSUE AS CONFIDENTAL)

#### Requester Info: 

+ Customer Email: 
+ Timezone: 
+ Training Requested: (NOTE: If the customer has requested multiple trainings, please make a separate issue for each.)


#### Support Team:
* [ ] Trainer: Send an email to the customer to schedule the training session(s). (Some trainings are two parts due to length. See [training page](https://about.gitlab.com/training/) for more about your specific training.)  You can use the following message template: 

```
Hi,

I'm <name>, and I'll be leading your <training name> training sessions. I'd like to schedule <number of sessions> with you. What's your availability like over the next few weeks? Additionally, all trainings are recorded for internal training and improvement purposes, if you'd like us to share these recordings with your team, please let me know! 

Best,
<You>
```
* [ ] Trainer: Add session(s) to the Shared "GitLab Support" Calendar with the format `TRAINER_NAME: Training_name with Company Name Session #`
* [ ] Trainer: Notify the support team that you'll be doing a training so new-hires/others can join.
* [ ] Trainer: Execute Training session(s). (See [training page](https://about.gitlab.com/training/) for more information and materials about your specific training.)
* [ ] Trainer: Post a Link to the training recording(s) to this issue.
* [ ] Trainer: Send the training videos to the customer for their records.
* [ ] Trainer: After all sessions have been completed, follow up with the customer for any feedback.
* [ ] Trainer: Post the feedback or "No feedback provided" message as a comment on the issue.
* [ ] Trainer: Ping the support lead to review the training issue so that it can be closed.
* [ ] Support Lead: Review all provided notes and videos if neccessary and make issues for needed improvements to the training process.
* [ ] Support Lead: Close issue :tada:
 
/assign @balameb

/label ~"Training"


